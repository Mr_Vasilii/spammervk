﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using static System.Net.Mime.MediaTypeNames;
using System.Linq;
using SpammerVK_PageObject;
using System.Windows.Forms;

namespace SpammerVK_PageUsers
{
    class PageUsers : PageObject
    {
        //private readonly By userFirstNameLocator = By.XPath("//*[@id=\"mcont\"]/div[2]/div/div/form/div[1]/input");
        private readonly By userFIOLocator = By.CssSelector("#mcont > div > div > div.PageBlock > div.owner_panel.profile_panel > div.pp_cont > h2");
        private readonly By buttonMassegeLocator = By.XPath("//*[@id=\"mcont\"]/div/div[2]/div[3]/div[2]/form/div[4]/button");
        private readonly By inputPhotoLocator = By.CssSelector("#mcont > div > div.messenger__layer.messenger__layer_convo > div.messenger__footer > div.messenger__write > form > div.uMailWrite__main.Mention_inited > div.uMailWrite__button.uMailWrite__buttonAttach.uMailWrite__button_disabled.false > div > div.Unfold__popup.ScrollView > div > div.Unfold__itemText > span.cp_icon_btn.cp_attach_btn.cp_inline_attach_btn > input.inline_upload");
        public bool SpammerMassege(string userUrl,string textMassenge)
        {
            GoToUrl(userUrl);
            
            IList<IWebElement> buttonMassenge = driver.FindElements(By.LinkText("Личное сообщение"));
            if (buttonMassenge.Count > 0)
            {
                string fio = driver.FindElement(userFIOLocator).Text;
                textMassenge = textMassenge.Replace("{fio}", fio);
                buttonMassenge.First().Click();

                IList<IWebElement> textArea = driver.FindElements(By.TagName("textarea"));
                while (textArea.Count == 0)
                {
                    Thread.Sleep(500);
                    textArea = driver.FindElements(By.TagName("textarea"));
                }

                textArea.First().SendKeys(textMassenge);
                Thread.Sleep(500);
                driver.FindElement(By.XPath("//*[@id=\"mcont\"]/div/div[2]/div[3]/div[2]/form/div[4]/button")).Click();


                return true;
            }
            return false;
        }
        public bool SpammerMassegeWhithPhoto(string userUrl, string textMassenge, string img)
        {
            GoToUrl(userUrl);
            
            
            IList<IWebElement> buttonMassenge = driver.FindElements(By.LinkText("Личное сообщение"));
            if (buttonMassenge.Count > 0)
            {
                string fio = driver.FindElement(userFIOLocator).Text;
                textMassenge = textMassenge.Replace("{fio}", fio);
                buttonMassenge.First().Click();

                IList<IWebElement> textArea = driver.FindElements(By.TagName("textarea"));
                while (textArea.Count == 0)
                {
                    Thread.Sleep(500);
                    textArea = driver.FindElements(By.TagName("textarea"));
                }
                driver.FindElement(inputPhotoLocator).SendKeys(img);
                textArea.First().SendKeys(textMassenge);
                Thread.Sleep(3000);
                driver.FindElement(By.XPath("//*[@id=\"mcont\"]/div/div[2]/div[3]/div[2]/form/div[4]/button")).Click();
                return true;
            }
            return false;
        }
    }
}
