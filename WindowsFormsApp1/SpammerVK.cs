﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using SpammerVK_PageObject;
using SpammerVK_PageLogin;
using SpammerVK_PageGroup;
using SpammerVK_PageUsers;
using OtherElementsIn;




namespace WindowsFormsApp1
{
    public partial class SpammerVK : Form
    {
        #region classes
        PageObject pageObject = new PageObject();
        PageLogin pageLogin = new PageLogin();
        PageGroup pageGroup = new PageGroup();
        PageUsers pageUsers = new PageUsers();
        OtherElements otherElement = new OtherElements();

        #endregion
        public IWebDriver driver;

        private int runCode;
        public SpammerVK()
        {
            InitializeComponent();
            this.progressBar1.ForeColor = Color.Red;
            this.buttonFindPeole.FlatAppearance.BorderSize = 0;
            this.buttonFindPeole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSpammerVK.FlatAppearance.BorderSize = 0;
            this.buttonSpammerVK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChecksAccountVK.FlatAppearance.BorderSize = 0;
            this.buttonChecksAccountVK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            GatheringGroupUsers.DoWork += backgroundWorker1_DoWork;
            GatheringGroupUsers.ProgressChanged += backgroundWorker1_ProgressChanged;
            GatheringGroupUsers.WorkerReportsProgress = true;
            labelQPosts.Visible = false;
            textBoxQPosts.Visible = false;
        }
        private void GiveAllClassWebDriver()
        {
            this.driver = pageObject.WebDriverUp();
            
            pageObject.TakeMeDriver(driver);
            pageLogin.TakeMeDriver(driver);
            pageGroup.TakeMeDriver(driver);
            pageUsers.TakeMeDriver(driver);
        }

        private void SpammerVK_Load(object sender, EventArgs e)
        {

        }

        private void ButtonBox_Enter(object sender, EventArgs e)
        {

        }

        private void ButtonFindPeole_Click(object sender, EventArgs e)
        {
            runCode = 1;
            groupFindPeople.Show();
            groupSpammerVK.Hide();
            groupChecksAccountVK.Hide();
        }

        private void ButtonSpammerVK_Click(object sender, EventArgs e)
        {
            runCode = 2;
            groupFindPeople.Hide();
            groupSpammerVK.Show();
            groupChecksAccountVK.Hide();
            
        }

        private void ButtonChecksAccountVK_Click(object sender, EventArgs e)
        {
            runCode = 3;
            groupFindPeople.Hide();
            groupSpammerVK.Hide();
            groupChecksAccountVK.Show();
            buttonBrute.Enabled = false;
        }

        private void ButtonFindPeopleGo_Click(object sender, EventArgs e)
        {
            groupFindPeople_1.Enabled = false;
            textBoxLog.Text = "";
            buttonChecksAccountVK.Enabled = false;
            buttonSpammerVK.Enabled = false;
            buttonFindPeole.Enabled = false;
            GatheringGroupUsers.RunWorkerAsync();
        }

       
        
        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            buttonChecksAccountVK.Enabled = false;
            buttonSpammerVK.Enabled = false;
            buttonFindPeole.Enabled = false;
            switch (runCode)
            {
                case 1:
                    GatheringGroupUsers.ReportProgress(0);
                    SetTextGatheringUsers("Процесс подключения WebDriver");
                    GiveAllClassWebDriver();
                    if (radioButtonAllUsersInGroup.Checked.ToString() == "True")
                        GatheringGroupAllUsers();
                    if (radioButtonLikes.Checked.ToString() == "True")
                        GatheringGroupPostsUsersLike();
                    if (radioButtonComments.Checked.ToString() == "True")
                        GatheringGroupPostsUsersComment();
                    break;
                case 2:
                    if (baseUrl == null || baseUrl.Length == 0)
                        MessageBox.Show("Нет базы пользователей!");
                    else
                    {
                        if (baseLogin == null || baseLogin.Length == 0)
                            MessageBox.Show("Нет базы аккаунтов!");
                        else
                        {
                            GatheringGroupUsers.ReportProgress(0);
                            SetTextGatheringUsers("Процесс подключения WebDriver");
                            GatheringGroupUsers.ReportProgress(1);
                            GiveAllClassWebDriver();
                            SpammerM();
                        }
                    }
                    break;

                case 3:
                    GatheringGroupUsers.ReportProgress(0);
                    SetTextGatheringUsers("Процесс подключения WebDriver");
                    GatheringGroupUsers.ReportProgress(1);
                    GiveAllClassWebDriver();
                    GiveLoginPassword();
                    break;
            }
            


            if (driver != null)
                driver.Quit();
            SetBla();
        }

        public void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            switch (runCode)
            {
                case 1:
                    progressBar1.Value = e.ProgressPercentage;
                    break;
                case 2:
                    progressBar2.Value = e.ProgressPercentage;
                    break;
                case 3:
                    progressBar3.Value = e.ProgressPercentage;
                    break;
            }
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            textBoxSpammLog.Text = "";
            buttonChecksAccountVK.Enabled = false;
            buttonSpammerVK.Enabled = false;
            buttonFindPeole.Enabled = false;
            buttonBaseUrl.Enabled = false;
            buttonBaseLogin.Enabled = false;
            buttonImg.Enabled = false;
            button1.Enabled = false;
            button3.Enabled = false;
            textBoxTextMasseng.ReadOnly = true;
            GatheringGroupUsers.RunWorkerAsync();
        }

        private void SpammerVK_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            if(driver != null)
                driver.Quit();
        }

        private void RadioButtonAllUsersInGroup_CheckedChanged(object sender, EventArgs e)
        {
            labelQPosts.Visible = false;
            textBoxQPosts.Visible = false;
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            labelQPosts.Visible = true;
            textBoxQPosts.Visible = true;
        }

        private void RadioButtonComments_CheckedChanged(object sender, EventArgs e)
        {
            labelQPosts.Visible = true;
            textBoxQPosts.Visible = true;
        }

        private void GroupFindPeople_Enter(object sender, EventArgs e)
        {

        }

        private void TextBoxLog_TextChanged(object sender, EventArgs e)
        {

        }

        private void GroupFindPeople_1_Enter(object sender, EventArgs e)
        {

        }

        private void GroupChecksAccountVK_Enter(object sender, EventArgs e)
        {

        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {

                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
                // получаем выбранный файл
                string filename = openFileDialog1.FileName;
            if(filename.Length == 0)
            {
                textBoxFileBase.Text = "Файл всё еще не выбран";
            }
            else
            {
                textBoxFileBase.Text = filename;
                SaveBase(filename);
                buttonBrute.Enabled = true;
            }
                
        }

        private void ButtonBrute_Click(object sender, EventArgs e)
        {
            textBoxBruteLog.Text = "";
            button2.Enabled = false;
            buttonBrute.Enabled = false;
            buttonChecksAccountVK.Enabled = false;
            buttonSpammerVK.Enabled = false;
            buttonFindPeole.Enabled = false;
            GatheringGroupUsers.RunWorkerAsync();
        }

        private void ButtonBaseUrl_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string fileUrl = openFileDialog1.FileName;
            if (fileUrl.Length == 0)
            {
                textBoxBaseUsers.Text = "Файл всё еще не выбран";
            }
            else
            {
                textBoxBaseUsers.Text = fileUrl;
                SaveBaseUrl(fileUrl);
            }
        }

        private void ButtonBaseLogin_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string fileLogins = openFileDialog1.FileName;
            if (fileLogins.Length == 0)
            {
                textBoxBaseLogins.Text = "Файл всё еще не выбран";
            }
            else
            {
                textBoxBaseLogins.Text = fileLogins;
                SaveBaseLogin(fileLogins);
            }
        }

        private void ButtonImg_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string fileImg = openFileDialog1.FileName;
            if (fileImg.Length == 0)
            {
                textBoxImg.Text = "Файл всё еще не выбран";
            }
            else
            {
                textBoxImg.Text = fileImg;
                SaveImg(fileImg);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            textBoxImg.Text = null;
            SaveImg(null);
        }
    }
}
