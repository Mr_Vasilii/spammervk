﻿using System;
using OpenQA.Selenium;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using SpammerVK_PageObject;
using System.Windows.Forms;
using WindowsFormsApp1;
using OtherElementsIn;


namespace SpammerVK_PageGroup
{
    
    class PageGroup : PageObject
    {
        
        private readonly By nameGroupLocator = By.XPath("//*[@id=\"mcont\"]/div/div[1]/div[1]/div[1]/div/h2");
        private readonly By buttonsQPageLocator = By.ClassName("pg_link");
        private readonly By postInGroup = By.CssSelector("div.wi_head > div > div.wi_info > a");
        public readonly By likesPostsInGroup = By.CssSelector("div.wi_body > div.wi_like_wrap > a");
        public readonly By buttonsQCommentLocator = By.ClassName("show_more");

        public string urlGroup;
        public void TakeMeUrlGroup(string url)
        {
            this.urlGroup = url;
        }
        public string NameGroup()
        {
            return driver.FindElement(nameGroupLocator).Text.ToString();
        }
        public string GoToGroup(string url)
        {
            url = "https://m." + url.Remove(0, url.IndexOf('v'));
            TakeMeUrlGroup(url);
            GoToUrl(url);
            LoadingPageState();
            return NameGroup();
        }
        public int GiveQuantityPage()
        {
            IList<IWebElement> checksUsers = driver.FindElements(By.CssSelector("#mcont > div > div > div > a.inline_item"));
            IList<IWebElement> checksButtons = driver.FindElements(buttonsQPageLocator);
            if (checksUsers.Count != 50)
            {
                return 1;
            }
            else
            {
                if (checksButtons.Count > 1)
                {
                    ClickElementWhithUseHrefLastElements(buttonsQPageLocator);
                    Thread.Sleep(1000);
                    LoadingPageState();
                    IList<IWebElement> buttonsQPage = driver.FindElements(buttonsQPageLocator);
                    var qP = buttonsQPage.LastOrDefault().Text;
                    var qPage = Convert.ToInt32(qP);
                    return qPage;
                }
                else
                    return 1;
            }
            
        }
        public List<string> GivePost(int quantityPost)
        {
            var k = 0;
            List<string> posts = new List<string>();
            IList <IWebElement> choosePosts = driver.FindElements(postInGroup);
            if (choosePosts.Count == 0)
                return posts;
            
            while(k < quantityPost)
            {
                k++;
                choosePosts = driver.FindElements(postInGroup);
                ScrollingJS_StaticPX();
            }
            k = 0;
            foreach(IWebElement x in choosePosts)
            {
                k++;
                posts.Add(x.GetAttribute("href"));
                if (k == quantityPost)
                    break;
            }
            return posts;
        }

        public void GiveLikesUsersInPost()
        {

            ClickElementWhithUseHref(likesPostsInGroup);

        }
    }
}
