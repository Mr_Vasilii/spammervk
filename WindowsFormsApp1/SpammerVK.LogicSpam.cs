﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class SpammerVK 
    {
        private string baseUrl;
        private string baseLogin;
        private string img;

        public void SaveBaseUrl(string text)
        {
            this.baseUrl = text;
        }
        public void SaveBaseLogin(string text)
        {
            this.baseLogin = text;
        }
        public void SaveImg(string text)
        {
            this.img = text;
        }

        public void SpammerM()
        {
            if (baseUrl == null || baseUrl.Length == 0)
            {
                MessageBox.Show("Нет базы пользователей!");
            }
            else
            {
                if (baseLogin == null || baseLogin.Length == 0)
                {
                    MessageBox.Show("Нет базы аккаунтов!");
                }
                else
                {
                    List<String> loginPassword = new List<String>();
                    using (StreamReader sr = File.OpenText(baseLogin))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            loginPassword.Add(s);
                        }
                    }
                    List<String> usersUrl = new List<String>();
                    using (StreamReader sr = File.OpenText(baseUrl))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            usersUrl.Add(s);
                        }
                    }
                    if (img == null || img.Length == 0)
                    {

                        //TODO

                        var spamMessage = textBoxTextMasseng.Text;
                        string loginPass, login, pass;
                        var startUrl = 0;
                        var check = 0;
                        bool checkMasseng;
                        int i = 0, j = 0;
                        var baseUrlCount = usersUrl.Count;
                        var baseLoginCount = loginPassword.Count;
                        for (i = 0; i < baseLoginCount; i++)
                        {
                            loginPass = loginPassword[i];
                            login = loginPass.Remove(loginPass.IndexOf(':'));
                            pass = loginPass.Remove(0, loginPass.IndexOf(':') + 1);
                            pageLogin.LoginVK(login, pass);
                            check = 0;
                            //TODO
                            if (pageLogin.CheckAcc(pageLogin.errorLoginLocator) == 1) SetTextGatheringUsers($"Был введен не верный логин или пароль у аккаунта - {login}");
                            else
                            {
                                if (pageLogin.CheckAcc(pageLogin.doublePassLoginLocator) == 1) SetTextGatheringUsers($"На аккаунте - {login}, стоит двойная аунтефикация");
                                else
                                {
                                    SetTextGatheringUsers($"Совершен вход на аккаунт - {login}");
                                    for (j = startUrl; j < baseUrlCount; j++)
                                    {

                                        SetTextGatheringUsers($"Переход на страницу - {usersUrl[j]}");
                                        checkMasseng = pageUsers.SpammerMassege(usersUrl[j], spamMessage);

                                        Thread.Sleep(1000);
                                        IList<IWebElement> checks = driver.FindElements(By.ClassName("msg__error"));
                                        if (checks.Count != 0)
                                        {
                                            check++;
                                            SetTextGatheringUsers($"Сообщение не отправленно");
                                            break;
                                        }
                                        else
                                        {
                                            if (checkMasseng == true)
                                                SetTextGatheringUsers($"Сообщение отправленно");
                                        }
                                        GatheringGroupUsers.ReportProgress((j * 100) / baseUrlCount);
                                        startUrl = j;
                                    }
                                }
                            }
                            if (check == 0)
                                break;
                            else
                            {
                                SetTextGatheringUsers($"Выход с аккаунта - {login}");
                                pageLogin.ExitVK();
                            }

                        }
                        if (i != baseLoginCount && j == baseUrlCount)
                        {
                            SetTextGatheringUsers($"Отправка сообщений завершена");
                            SetTextGatheringUsers($"У Вас ещё остались аккаунты ВК");
                            File.Delete("Остаток аккаунтов ВК");
                            StreamWriter sw = otherElement.CreateNewFile("Остаток аккаунтов ВК.xml");

                            for (int x = i; x < baseLoginCount; x++)
                            {
                                sw.WriteLine(loginPassword[x]);
                            }
                            sw.Close(); GatheringGroupUsers.ReportProgress(100);
                            SetTextGatheringUsers($"Оставшиеся аккаунты ВК сохранены в - Остаток аккаунтов ВК.xml");
                        }
                        else
                        {
                            if (i == baseLoginCount && j != baseUrlCount)
                            {

                                SetTextGatheringUsers($"Отправка сообщений завершена");
                                SetTextGatheringUsers($"У Вас аккаунтов ВК меньше чем пользователей");
                                File.Delete("Остаток пользователей");
                                StreamWriter sw = otherElement.CreateNewFile("Остаток пользователей.xml");

                                for (int y = j; y < baseUrlCount; y++)
                                {
                                    sw.WriteLine(usersUrl[y]);
                                }
                                sw.Close(); GatheringGroupUsers.ReportProgress(100);
                                SetTextGatheringUsers($"Оставшиеся пользователи сохранены в - Остаток пользователей.xml");

                            }
                        }

                    }
                    else
                    {
                        //TODO
                        var spamMessage = textBoxTextMasseng.Text;
                        string loginPass, login, pass;
                        var startUrl = 0;
                        var check = 0;
                        bool checkMasseng;
                        int i=0, j=0;
                        var baseUrlCount = usersUrl.Count;
                        var baseLoginCount = loginPassword.Count;
                        for (i = 0; i < baseLoginCount; i++)
                        {
                            loginPass = loginPassword[i];
                            login = loginPass.Remove(loginPass.IndexOf(':'));
                            pass = loginPass.Remove(0, loginPass.IndexOf(':') + 1);
                            pageLogin.LoginVK(login, pass);
                            check = 0;
                            //TODO
                            if (pageLogin.CheckAcc(pageLogin.errorLoginLocator) == 1) SetTextGatheringUsers($"Был введен не верный логин или пароль у аккаунта - {login}");
                            else
                            {
                                if (pageLogin.CheckAcc(pageLogin.doublePassLoginLocator) == 1) SetTextGatheringUsers($"На аккаунте - {login}, стоит двойная аунтефикация");
                                else
                                {
                                    SetTextGatheringUsers($"Совершен вход на аккаунт - {login}");
                                    for (j = startUrl; j < baseUrlCount; j++)
                                    {

                                        SetTextGatheringUsers($"Переход на страницу - {usersUrl[j]}");
                                        checkMasseng = pageUsers.SpammerMassegeWhithPhoto(usersUrl[j], spamMessage, img);
                                        
                                        Thread.Sleep(1000);
                                        IList<IWebElement> checks = driver.FindElements(By.ClassName("msg__error"));
                                        if (checks.Count != 0)
                                        {
                                            check++;
                                            SetTextGatheringUsers($"Сообщение не отправленно");
                                            break;
                                        }
                                        else
                                        {
                                            if(checkMasseng == true)
                                                SetTextGatheringUsers($"Сообщение отправленно");
                                        }
                                        GatheringGroupUsers.ReportProgress((j*100)/baseUrlCount);
                                        startUrl = j;
                                    }
                                }
                            }
                            if (check == 0)
                                break;
                            else
                            {
                                SetTextGatheringUsers($"Выход с аккаунта - {login}");
                                pageLogin.ExitVK();
                            }  
                            
                        }
                        if(i != baseLoginCount && j == baseUrlCount)
                        {
                            SetTextGatheringUsers($"Отправка сообщений завершена");
                            SetTextGatheringUsers($"У Вас ещё остались аккаунты ВК");
                            File.Delete("Остаток аккаунтов ВК");
                            StreamWriter sw = otherElement.CreateNewFile("Остаток аккаунтов ВК.xml");
                            
                            for (int x = i; x < baseLoginCount; x++)
                            {
                                sw.WriteLine(loginPassword[x]);
                            }
                            sw.Close(); GatheringGroupUsers.ReportProgress(100);
                            SetTextGatheringUsers($"Оставшиеся аккаунты ВК сохранены в - Остаток аккаунтов ВК.xml");
                        }
                        else
                        {
                            if (i == baseLoginCount && j != baseUrlCount)
                            {
                                
                                SetTextGatheringUsers($"Отправка сообщений завершена");
                                SetTextGatheringUsers($"У Вас аккаунтов ВК меньше чем пользователей");
                                File.Delete("Остаток пользователей");
                                StreamWriter sw = otherElement.CreateNewFile("Остаток пользователей.xml");
                                
                                for (int y = j; y < baseUrlCount; y++)
                                {
                                    sw.WriteLine(usersUrl[y]);
                                }
                                sw.Close(); GatheringGroupUsers.ReportProgress(100);
                                SetTextGatheringUsers($"Оставшиеся пользователи сохранены в - Остаток пользователей.xml");

                            }
                        }
                        
                        
                    }
                }
            }
        }


    }
}
