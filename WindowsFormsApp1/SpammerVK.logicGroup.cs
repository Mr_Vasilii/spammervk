﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApp1
{

    public partial class SpammerVK
    {
        private delegate void SafeCallDelegate(string text);
        public void GatheringGroupAllUsers()
        {
            SetTextGatheringUsers("Выполняется вход на сайт ВКонтакте");
            pageLogin.LoginVK(textBoxLogin.Text.ToString(), textBoxPassword.Text.ToString());
            if (pageLogin.CheckAcc(pageLogin.errorLoginLocator) == 1) { MessageBox.Show("Был введен не верный логин или пароль"); SetTextGatheringUsers("Был введен не верный логин или пароль"); }
            else
            {
                if (pageLogin.CheckAcc(pageLogin.doublePassLoginLocator) == 1) { MessageBox.Show("На аккаунте стоит двойная аунтефикация"); SetTextGatheringUsers("На аккаунте стоит двойная аунтефикация"); }
                else
                {
                    SetTextGatheringUsers("Выполняется переход на страницу группы и сбор имени группы");
                    var nameGroup = pageGroup.GoToGroup(textBoxGroupVK.Text.ToString()) + " - Все пользователи.xml";
                    SetTextGatheringUsers("Создание текстового файла для хранения ссылок на пользователя");
                    StreamWriter sw = otherElement.CreateNewFile(nameGroup);
                    pageGroup.GoToUrl(pageGroup.urlGroup + "?act=members");
                    var qPage = pageGroup.GiveQuantityPage();
                    SetTextGatheringUsers("Выполняется расчёт сколько страниц пользователей - " + qPage.ToString());
                    for (int i = qPage - 1; i > -1; i--)
                    {
                        SetTextGatheringUsers("Выполняется сбор пользователйс со страницы - " + (i + 1).ToString());
                        IList<IWebElement> checks = driver.FindElements(By.CssSelector("#mcont > div > div > div > a.inline_item"));
                        foreach (IWebElement x in checks)
                        {
                            sw.WriteLine(x.GetAttribute("href"));
                        }
                        GatheringGroupUsers.ReportProgress(99 - ((i * 100) / qPage));
                        if (i < 1)
                        {
                            break;
                        }
                        else
                        {
                            pageObject.ClickElementWhithUseHref(By.LinkText(i.ToString()));
                            pageObject.LoadingPageState();
                            driver.Navigate().Refresh();
                            pageObject.LoadingPageState();
                        }
                    }
                    GatheringGroupUsers.ReportProgress(100);
                    SetTextGatheringUsers("Сбор пользователй завершён");
                    sw.Close();
                    otherElement.ClearDuplicateUrl(nameGroup);
                    SetTextGatheringUsers("Все ссылки на пользователей были сохранены в файле - " + nameGroup);
                }
            }
            


        }
        public void GatheringGroupPostsUsersLike()
        {
            var qPosts = Convert.ToInt32(textBoxQPosts.Text.ToString());
            var k = 0;
            SetTextGatheringUsers("Выполняется вход на сайт ВКонтакте");
            pageLogin.LoginVK(textBoxLogin.Text.ToString(), textBoxPassword.Text.ToString());
            SetTextGatheringUsers("Выполняется переход на страницу группы и сбор имени группы");
            var nameGroup = pageGroup.GoToGroup(textBoxGroupVK.Text.ToString()) + " - Like.xml";
            List<string> posts = pageGroup.GivePost(qPosts);
            if (posts.Count == 0)
                MessageBox.Show("В группе нет постов!");
            else
            {
                SetTextGatheringUsers("Создание текстового файла для хранения ссылок на пользователя");
                StreamWriter sw = otherElement.CreateNewFile(nameGroup);
                foreach (string x in posts)
                {
                    k++;
                    SetTextGatheringUsers(("Заходим на " + k + " пост"));
                    pageObject.GoToUrl(x);
                    if(pageObject.ChecksEllementInPage(pageGroup.likesPostsInGroup) == true)
                    {
                        pageObject.ClickElementWhithUseHref(pageGroup.likesPostsInGroup);
                        var qPage = pageGroup.GiveQuantityPage();
                        SetTextGatheringUsers("Выполняется расчёт сколько страниц пользователей - " + qPage.ToString());
                        for (int i = qPage - 1; i > -1; i--)
                        {
                            SetTextGatheringUsers("Выполняется сбор пользователйс со страницы - " + (i + 1).ToString());
                            IList<IWebElement> checks = driver.FindElements(By.CssSelector("#mcont > div > div > div > div > a.inline_item"));
                            foreach (IWebElement y in checks)
                            {
                                sw.WriteLine(y.GetAttribute("href"));
                            }
                            GatheringGroupUsers.ReportProgress(((98 - ((i * 100) / qPage)) / qPosts) * k);
                            if (i < 1)
                            {
                                break;
                            }
                            else
                            {
                                pageObject.ClickElementWhithUseHref(By.LinkText(i.ToString()));
                                pageObject.LoadingPageState();
                                driver.Navigate().Refresh();
                                pageObject.LoadingPageState();
                            }
                        }
                    }
                    
                }
                sw.Close();
                otherElement.ClearDuplicateUrl(nameGroup);
                GatheringGroupUsers.ReportProgress(100);
                SetTextGatheringUsers("Сбор пользователй завершён");
                SetTextGatheringUsers("Все ссылки на пользователей были сохранены в файле - " + nameGroup);
            }
        }

        public void GatheringGroupPostsUsersComment() 
        {
            var qPosts = Convert.ToInt32(textBoxQPosts.Text.ToString());
            var k = 0;
            SetTextGatheringUsers("Выполняется вход на сайт ВКонтакте");
            pageLogin.LoginVK(textBoxLogin.Text.ToString(), textBoxPassword.Text.ToString());
            SetTextGatheringUsers("Выполняется переход на страницу группы и сбор имени группы");
            var nameGroup = pageGroup.GoToGroup(textBoxGroupVK.Text.ToString()) + " - Comments.xml";
            List<string> posts = pageGroup.GivePost(qPosts);
            if (posts.Count == 0)
                MessageBox.Show("В группе нет постов!");
            else
            {
                SetTextGatheringUsers("Создание текстового файла для хранения ссылок на пользователя");
                StreamWriter sw = otherElement.CreateNewFile(nameGroup);
                foreach (string x in posts)
                {
                    k++;
                    SetTextGatheringUsers(("Сбор пользователей с коментраиев к посту - " + k));
                    pageObject.GoToUrl(x);
                    while (pageObject.ChecksEllementInPage(pageGroup.buttonsQCommentLocator) == true)
                    {
                        pageObject.ClickElementWhithUseHref(pageGroup.buttonsQCommentLocator);
                        IList<IWebElement> checks = driver.FindElements(By.CssSelector("div > div.pi_head > a"));
                        foreach (IWebElement y in checks)
                        {
                            sw.WriteLine(y.GetAttribute("href"));
                        }
                    }
                    GatheringGroupUsers.ReportProgress((98/qPosts) * k);

                }
                sw.Close();
                otherElement.ClearDuplicateUrl(nameGroup);
                GatheringGroupUsers.ReportProgress(100);
                SetTextGatheringUsers("Сбор пользователй завершён");
                
                SetTextGatheringUsers("Все ссылки на пользователей были сохранены в файле - " + nameGroup);
            }
        }
        private void SetBla()
        {
            
            switch (runCode)
            {
                case 1:
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => groupFindPeople_1.Enabled = s), true);
                    else
                        groupFindPeople.Enabled = true;
                    break;
                case 2:
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => buttonBaseUrl.Enabled = s), true);
                    else
                        buttonBaseUrl.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => buttonBaseLogin.Enabled = s), true);
                    else
                        buttonBaseLogin.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => buttonImg.Enabled = s), true);
                    else
                        buttonImg.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => button1.Enabled = s), true);
                    else
                        button1.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => button3.Enabled = s), true);
                    else
                        button3.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => textBoxTextMasseng.ReadOnly = s), false);
                    else
                        textBoxTextMasseng.ReadOnly = false;
                    break;
                case 3:
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => button2.Enabled = s), true);
                    else
                        button2.Enabled = true;
                    if (InvokeRequired)
                        Invoke(new Action<bool>((s) => buttonBrute.Enabled = s), true);
                    else
                        buttonBrute.Enabled = true;
                    break;
            }
            if (InvokeRequired)
                Invoke(new Action<bool>((s) => buttonFindPeole.Enabled = s), true);
            else
                buttonFindPeole.Enabled = true;
            if (InvokeRequired)
                Invoke(new Action<bool>((s) => buttonSpammerVK.Enabled = s), true);
            else
                buttonSpammerVK.Enabled = true;
            if (InvokeRequired)
                Invoke(new Action<bool>((s) => buttonChecksAccountVK.Enabled = s), true);
            else
                buttonChecksAccountVK.Enabled = true;
        }
        private void SetTextGatheringUsers(string str)
        {
            switch (runCode)
            {
                case 1:
                    str += "\r\n";
                    if (InvokeRequired)
                        Invoke(new Action<string>((s) => textBoxLog.AppendText(s)), str);
                    else
                        textBoxLog.AppendText(str);
                    break;
                case 2:
                    str += "\r\n";
                    if (InvokeRequired)
                        Invoke(new Action<string>((s) => textBoxSpammLog.AppendText(s)), str);
                    else
                        textBoxLog.AppendText(str);
                    break;
                case 3:
                    str += "\r\n";
                    if (InvokeRequired)
                        Invoke(new Action<string>((s) => textBoxBruteLog.AppendText(s)), str);
                    else
                        textBoxBruteLog.AppendText(str);
                    break;
            }
            
        }
    }
}
