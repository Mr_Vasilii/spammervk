﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtherElementsIn
{
    class OtherElements
    {
        public StreamWriter CreateNewFile(string text)
        {
            FileStream aFile = new FileStream((text), FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(aFile);
            aFile.Seek(0, SeekOrigin.End);
            return sw;
        }
        public void ClearDuplicateUrl(string text)
        {
            List<String> urlPeople = new List<String>();
            using (StreamReader sr = File.OpenText(text))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    urlPeople.Add(s);
                }
            }
            File.Delete(text);
            IEnumerable<String> distinctUrlPeople = urlPeople.Distinct();
            FileStream aFile = new FileStream((text), FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(aFile);
            aFile.Seek(0, SeekOrigin.End);
            foreach (String x in distinctUrlPeople)
                sw.WriteLine(x);
            sw.Close();
        }
    }
}
