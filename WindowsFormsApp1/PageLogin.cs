﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using static System.Net.Mime.MediaTypeNames;
using System.Linq;
using SpammerVK_PageObject;
using System.Windows.Forms;


namespace SpammerVK_PageLogin
{
    class PageLogin : PageObject

    {
        private readonly By loginLocator = By.XPath("//*[@id=\"mcont\"]/div[2]/div/div/form/dl[1]/dd/input");
        private readonly By passwordLocator = By.XPath("//*[@id=\"mcont\"]/div[2]/div/div/form/dl[2]/dd/input");
        private readonly By buttonLoginLocator = By.XPath("//*[@id=\"mcont\"]/div[2]/div/div/form/div[1]/input");
        private readonly By buttonExit = By.CssSelector("#lm_cont > div.mfoot > ul > li.mmi_logout > a");

        public readonly By errorLoginLocator = By.CssSelector("#mcont > div.pcont.fit_box.bl_cont.new_form > div > div > div.fi_row > div > div");
        public readonly By doublePassLoginLocator = By.CssSelector("#mcont > div > div > div > form > dl > dd > div.iwrap > input");
        public void LoginVK(String login, String password)
        {
            GoToUrl("https://m.vk.com/");
            InputElement(loginLocator, login);
            InputElement(passwordLocator, password);
            //Thread.Sleep(500);
            ClickElement(buttonLoginLocator);
            LoadingPageState();
        }

        public int CheckAcc(By check)
        {
            IList<IWebElement> checks = driver.FindElements(check);
            if (checks.Count != 0)
            {
                return 1;
            }
            return 0;
        }

        public void ExitVK()
        {
            ClickElementWhithUseHref(buttonExit);
        }

    }
}
