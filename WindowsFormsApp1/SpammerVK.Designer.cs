﻿namespace WindowsFormsApp1
{
    partial class SpammerVK
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpammerVK));
            this.buttonBox = new System.Windows.Forms.GroupBox();
            this.buttonChecksAccountVK = new System.Windows.Forms.Button();
            this.buttonSpammerVK = new System.Windows.Forms.Button();
            this.buttonFindPeole = new System.Windows.Forms.Button();
            this.groupFindPeople = new System.Windows.Forms.GroupBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.groupFindPeople_1 = new System.Windows.Forms.GroupBox();
            this.radioButtonComments = new System.Windows.Forms.RadioButton();
            this.labelQPosts = new System.Windows.Forms.Label();
            this.textBoxQPosts = new System.Windows.Forms.TextBox();
            this.radioButtonLikes = new System.Windows.Forms.RadioButton();
            this.radioButtonAllUsersInGroup = new System.Windows.Forms.RadioButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonFindPeopleGo = new System.Windows.Forms.Button();
            this.textBoxGroupVK = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupSpammerVK = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupChecksAccountVK = new System.Windows.Forms.GroupBox();
            this.GatheringGroupUsers = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxFileBase = new System.Windows.Forms.TextBox();
            this.buttonBrute = new System.Windows.Forms.Button();
            this.textBoxBruteLog = new System.Windows.Forms.TextBox();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonBaseUrl = new System.Windows.Forms.Button();
            this.buttonBaseLogin = new System.Windows.Forms.Button();
            this.buttonImg = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.textBoxBaseUsers = new System.Windows.Forms.TextBox();
            this.textBoxBaseLogins = new System.Windows.Forms.TextBox();
            this.textBoxImg = new System.Windows.Forms.TextBox();
            this.textBoxTextMasseng = new System.Windows.Forms.TextBox();
            this.textBoxSpammLog = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonBox.SuspendLayout();
            this.groupFindPeople.SuspendLayout();
            this.groupFindPeople_1.SuspendLayout();
            this.groupSpammerVK.SuspendLayout();
            this.groupChecksAccountVK.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBox
            // 
            this.buttonBox.Controls.Add(this.buttonChecksAccountVK);
            this.buttonBox.Controls.Add(this.buttonSpammerVK);
            this.buttonBox.Controls.Add(this.buttonFindPeole);
            this.buttonBox.Location = new System.Drawing.Point(13, 5);
            this.buttonBox.Name = "buttonBox";
            this.buttonBox.Size = new System.Drawing.Size(115, 450);
            this.buttonBox.TabIndex = 0;
            this.buttonBox.TabStop = false;
            this.buttonBox.Enter += new System.EventHandler(this.ButtonBox_Enter);
            // 
            // buttonChecksAccountVK
            // 
            this.buttonChecksAccountVK.BackColor = System.Drawing.SystemColors.Control;
            this.buttonChecksAccountVK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonChecksAccountVK.BackgroundImage")));
            this.buttonChecksAccountVK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonChecksAccountVK.Location = new System.Drawing.Point(17, 310);
            this.buttonChecksAccountVK.Name = "buttonChecksAccountVK";
            this.buttonChecksAccountVK.Size = new System.Drawing.Size(80, 80);
            this.buttonChecksAccountVK.TabIndex = 2;
            this.buttonChecksAccountVK.UseVisualStyleBackColor = false;
            this.buttonChecksAccountVK.Click += new System.EventHandler(this.ButtonChecksAccountVK_Click);
            // 
            // buttonSpammerVK
            // 
            this.buttonSpammerVK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSpammerVK.BackgroundImage")));
            this.buttonSpammerVK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSpammerVK.Location = new System.Drawing.Point(17, 180);
            this.buttonSpammerVK.Name = "buttonSpammerVK";
            this.buttonSpammerVK.Size = new System.Drawing.Size(80, 80);
            this.buttonSpammerVK.TabIndex = 1;
            this.buttonSpammerVK.UseVisualStyleBackColor = true;
            this.buttonSpammerVK.Click += new System.EventHandler(this.ButtonSpammerVK_Click);
            // 
            // buttonFindPeole
            // 
            this.buttonFindPeole.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonFindPeole.BackgroundImage")));
            this.buttonFindPeole.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFindPeole.Location = new System.Drawing.Point(17, 50);
            this.buttonFindPeole.Name = "buttonFindPeole";
            this.buttonFindPeole.Size = new System.Drawing.Size(80, 80);
            this.buttonFindPeole.TabIndex = 0;
            this.buttonFindPeole.UseVisualStyleBackColor = true;
            this.buttonFindPeole.Click += new System.EventHandler(this.ButtonFindPeole_Click);
            // 
            // groupFindPeople
            // 
            this.groupFindPeople.Controls.Add(this.textBoxLog);
            this.groupFindPeople.Controls.Add(this.groupFindPeople_1);
            this.groupFindPeople.Location = new System.Drawing.Point(140, 5);
            this.groupFindPeople.Name = "groupFindPeople";
            this.groupFindPeople.Size = new System.Drawing.Size(487, 450);
            this.groupFindPeople.TabIndex = 1;
            this.groupFindPeople.TabStop = false;
            this.groupFindPeople.Text = "Сбор участников группы";
            this.groupFindPeople.Visible = false;
            this.groupFindPeople.Enter += new System.EventHandler(this.GroupFindPeople_Enter);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(212, 193);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLog.Size = new System.Drawing.Size(257, 197);
            this.textBoxLog.TabIndex = 11;
            this.textBoxLog.TextChanged += new System.EventHandler(this.TextBoxLog_TextChanged);
            // 
            // groupFindPeople_1
            // 
            this.groupFindPeople_1.Controls.Add(this.radioButtonComments);
            this.groupFindPeople_1.Controls.Add(this.labelQPosts);
            this.groupFindPeople_1.Controls.Add(this.textBoxQPosts);
            this.groupFindPeople_1.Controls.Add(this.radioButtonLikes);
            this.groupFindPeople_1.Controls.Add(this.radioButtonAllUsersInGroup);
            this.groupFindPeople_1.Controls.Add(this.progressBar1);
            this.groupFindPeople_1.Controls.Add(this.label4);
            this.groupFindPeople_1.Controls.Add(this.buttonFindPeopleGo);
            this.groupFindPeople_1.Controls.Add(this.textBoxGroupVK);
            this.groupFindPeople_1.Controls.Add(this.label3);
            this.groupFindPeople_1.Controls.Add(this.textBoxPassword);
            this.groupFindPeople_1.Controls.Add(this.label2);
            this.groupFindPeople_1.Controls.Add(this.textBoxLogin);
            this.groupFindPeople_1.Controls.Add(this.label1);
            this.groupFindPeople_1.Location = new System.Drawing.Point(0, 0);
            this.groupFindPeople_1.Name = "groupFindPeople_1";
            this.groupFindPeople_1.Size = new System.Drawing.Size(487, 450);
            this.groupFindPeople_1.TabIndex = 1;
            this.groupFindPeople_1.TabStop = false;
            this.groupFindPeople_1.Text = "Сбор участников группы";
            this.groupFindPeople_1.Enter += new System.EventHandler(this.GroupFindPeople_1_Enter);
            // 
            // radioButtonComments
            // 
            this.radioButtonComments.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButtonComments.Location = new System.Drawing.Point(11, 310);
            this.radioButtonComments.Name = "radioButtonComments";
            this.radioButtonComments.Size = new System.Drawing.Size(180, 48);
            this.radioButtonComments.TabIndex = 17;
            this.radioButtonComments.TabStop = true;
            this.radioButtonComments.Text = "Сбор пользователей которые прокоментировали посты";
            this.radioButtonComments.UseVisualStyleBackColor = true;
            this.radioButtonComments.CheckedChanged += new System.EventHandler(this.RadioButtonComments_CheckedChanged);
            // 
            // labelQPosts
            // 
            this.labelQPosts.AutoSize = true;
            this.labelQPosts.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelQPosts.Location = new System.Drawing.Point(18, 361);
            this.labelQPosts.Name = "labelQPosts";
            this.labelQPosts.Size = new System.Drawing.Size(154, 13);
            this.labelQPosts.TabIndex = 16;
            this.labelQPosts.Text = "Введите колличество постов";
            // 
            // textBoxQPosts
            // 
            this.textBoxQPosts.Location = new System.Drawing.Point(178, 358);
            this.textBoxQPosts.Name = "textBoxQPosts";
            this.textBoxQPosts.Size = new System.Drawing.Size(29, 20);
            this.textBoxQPosts.TabIndex = 15;
            this.textBoxQPosts.Text = "10";
            this.textBoxQPosts.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radioButtonLikes
            // 
            this.radioButtonLikes.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButtonLikes.Location = new System.Drawing.Point(11, 266);
            this.radioButtonLikes.Name = "radioButtonLikes";
            this.radioButtonLikes.Size = new System.Drawing.Size(190, 53);
            this.radioButtonLikes.TabIndex = 13;
            this.radioButtonLikes.TabStop = true;
            this.radioButtonLikes.Text = "Сбор пользователей которые поставили Like на постах";
            this.radioButtonLikes.UseVisualStyleBackColor = true;
            this.radioButtonLikes.CheckedChanged += new System.EventHandler(this.RadioButton2_CheckedChanged);
            // 
            // radioButtonAllUsersInGroup
            // 
            this.radioButtonAllUsersInGroup.Checked = true;
            this.radioButtonAllUsersInGroup.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButtonAllUsersInGroup.Location = new System.Drawing.Point(11, 230);
            this.radioButtonAllUsersInGroup.Name = "radioButtonAllUsersInGroup";
            this.radioButtonAllUsersInGroup.Size = new System.Drawing.Size(195, 17);
            this.radioButtonAllUsersInGroup.TabIndex = 12;
            this.radioButtonAllUsersInGroup.TabStop = true;
            this.radioButtonAllUsersInGroup.Text = "Сбор всех пользователей группы";
            this.radioButtonAllUsersInGroup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButtonAllUsersInGroup.UseVisualStyleBackColor = true;
            this.radioButtonAllUsersInGroup.CheckedChanged += new System.EventHandler(this.RadioButtonAllUsersInGroup_CheckedChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.progressBar1.Location = new System.Drawing.Point(0, 440);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(487, 4);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(206, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 9;
            // 
            // buttonFindPeopleGo
            // 
            this.buttonFindPeopleGo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFindPeopleGo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonFindPeopleGo.Location = new System.Drawing.Point(126, 399);
            this.buttonFindPeopleGo.Name = "buttonFindPeopleGo";
            this.buttonFindPeopleGo.Size = new System.Drawing.Size(229, 35);
            this.buttonFindPeopleGo.TabIndex = 6;
            this.buttonFindPeopleGo.Text = "Начать сбор пользователей";
            this.buttonFindPeopleGo.UseVisualStyleBackColor = true;
            this.buttonFindPeopleGo.Click += new System.EventHandler(this.ButtonFindPeopleGo_Click);
            // 
            // textBoxGroupVK
            // 
            this.textBoxGroupVK.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxGroupVK.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApp1.Properties.Settings.Default, "GroupVK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxGroupVK.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxGroupVK.ForeColor = System.Drawing.Color.Black;
            this.textBoxGroupVK.Location = new System.Drawing.Point(95, 144);
            this.textBoxGroupVK.Name = "textBoxGroupVK";
            this.textBoxGroupVK.Size = new System.Drawing.Size(250, 23);
            this.textBoxGroupVK.TabIndex = 5;
            this.textBoxGroupVK.Text = global::WindowsFormsApp1.Properties.Settings.Default.GroupVK;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(18, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "VK Group";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxPassword.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApp1.Properties.Settings.Default, "PasswordVK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxPassword.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPassword.ForeColor = System.Drawing.Color.Black;
            this.textBoxPassword.Location = new System.Drawing.Point(95, 80);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(250, 23);
            this.textBoxPassword.TabIndex = 3;
            this.textBoxPassword.Text = global::WindowsFormsApp1.Properties.Settings.Default.PasswordVK;
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(18, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxLogin.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApp1.Properties.Settings.Default, "loginVK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxLogin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLogin.ForeColor = System.Drawing.Color.Black;
            this.textBoxLogin.Location = new System.Drawing.Point(95, 47);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(250, 23);
            this.textBoxLogin.TabIndex = 1;
            this.textBoxLogin.Text = global::WindowsFormsApp1.Properties.Settings.Default.loginVK;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(18, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login";
            // 
            // groupSpammerVK
            // 
            this.groupSpammerVK.Controls.Add(this.button3);
            this.groupSpammerVK.Controls.Add(this.label8);
            this.groupSpammerVK.Controls.Add(this.textBoxTextMasseng);
            this.groupSpammerVK.Controls.Add(this.textBoxSpammLog);
            this.groupSpammerVK.Controls.Add(this.textBoxImg);
            this.groupSpammerVK.Controls.Add(this.textBoxBaseLogins);
            this.groupSpammerVK.Controls.Add(this.textBoxBaseUsers);
            this.groupSpammerVK.Controls.Add(this.progressBar2);
            this.groupSpammerVK.Controls.Add(this.buttonImg);
            this.groupSpammerVK.Controls.Add(this.buttonBaseLogin);
            this.groupSpammerVK.Controls.Add(this.buttonBaseUrl);
            this.groupSpammerVK.Controls.Add(this.button1);
            this.groupSpammerVK.Controls.Add(this.label5);
            this.groupSpammerVK.Location = new System.Drawing.Point(140, 5);
            this.groupSpammerVK.Name = "groupSpammerVK";
            this.groupSpammerVK.Size = new System.Drawing.Size(487, 450);
            this.groupSpammerVK.TabIndex = 2;
            this.groupSpammerVK.TabStop = false;
            this.groupSpammerVK.Text = "Отправка спамма";
            this.groupSpammerVK.Visible = false;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(126, 97);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(199, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Начать рассылку сообщений";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // groupChecksAccountVK
            // 
            this.groupChecksAccountVK.Controls.Add(this.label7);
            this.groupChecksAccountVK.Controls.Add(this.label6);
            this.groupChecksAccountVK.Controls.Add(this.progressBar3);
            this.groupChecksAccountVK.Controls.Add(this.textBoxBruteLog);
            this.groupChecksAccountVK.Controls.Add(this.buttonBrute);
            this.groupChecksAccountVK.Controls.Add(this.textBoxFileBase);
            this.groupChecksAccountVK.Controls.Add(this.button2);
            this.groupChecksAccountVK.Location = new System.Drawing.Point(140, 5);
            this.groupChecksAccountVK.Name = "groupChecksAccountVK";
            this.groupChecksAccountVK.Size = new System.Drawing.Size(487, 450);
            this.groupChecksAccountVK.TabIndex = 2;
            this.groupChecksAccountVK.TabStop = false;
            this.groupChecksAccountVK.Text = "Проверка аккаунтов VK";
            this.groupChecksAccountVK.Visible = false;
            this.groupChecksAccountVK.Enter += new System.EventHandler(this.GroupChecksAccountVK_Enter);
            // 
            // GatheringGroupUsers
            // 
            this.GatheringGroupUsers.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker1_DoWork);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog1_FileOk);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(309, 42);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Загрузить Базу";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // textBoxFileBase
            // 
            this.textBoxFileBase.Location = new System.Drawing.Point(21, 42);
            this.textBoxFileBase.Name = "textBoxFileBase";
            this.textBoxFileBase.ReadOnly = true;
            this.textBoxFileBase.Size = new System.Drawing.Size(254, 20);
            this.textBoxFileBase.TabIndex = 1;
            this.textBoxFileBase.Text = "Bыберите файл";
            // 
            // buttonBrute
            // 
            this.buttonBrute.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonBrute.Location = new System.Drawing.Point(75, 121);
            this.buttonBrute.Name = "buttonBrute";
            this.buttonBrute.Size = new System.Drawing.Size(293, 23);
            this.buttonBrute.TabIndex = 2;
            this.buttonBrute.Text = "Начать проверку аккаунтов";
            this.buttonBrute.UseVisualStyleBackColor = true;
            this.buttonBrute.Click += new System.EventHandler(this.ButtonBrute_Click);
            // 
            // textBoxBruteLog
            // 
            this.textBoxBruteLog.Location = new System.Drawing.Point(21, 165);
            this.textBoxBruteLog.Multiline = true;
            this.textBoxBruteLog.Name = "textBoxBruteLog";
            this.textBoxBruteLog.ReadOnly = true;
            this.textBoxBruteLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBruteLog.Size = new System.Drawing.Size(405, 238);
            this.textBoxBruteLog.TabIndex = 3;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(0, 440);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(487, 4);
            this.progressBar3.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(18, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(418, 38);
            this.label6.TabIndex = 5;
            this.label6.Text = "Логина и пароль от контакта должны быть разделены занок двоеточия    ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("NSimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(286, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = ":";
            // 
            // buttonBaseUrl
            // 
            this.buttonBaseUrl.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonBaseUrl.Location = new System.Drawing.Point(279, 20);
            this.buttonBaseUrl.Name = "buttonBaseUrl";
            this.buttonBaseUrl.Size = new System.Drawing.Size(190, 23);
            this.buttonBaseUrl.TabIndex = 1;
            this.buttonBaseUrl.Text = "Загрузить базу пользователей";
            this.buttonBaseUrl.UseVisualStyleBackColor = true;
            this.buttonBaseUrl.Click += new System.EventHandler(this.ButtonBaseUrl_Click);
            // 
            // buttonBaseLogin
            // 
            this.buttonBaseLogin.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonBaseLogin.Location = new System.Drawing.Point(279, 45);
            this.buttonBaseLogin.Name = "buttonBaseLogin";
            this.buttonBaseLogin.Size = new System.Drawing.Size(190, 23);
            this.buttonBaseLogin.TabIndex = 2;
            this.buttonBaseLogin.Text = "Загрузить базу аккаунтов";
            this.buttonBaseLogin.UseVisualStyleBackColor = true;
            this.buttonBaseLogin.Click += new System.EventHandler(this.ButtonBaseLogin_Click);
            // 
            // buttonImg
            // 
            this.buttonImg.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonImg.Location = new System.Drawing.Point(279, 71);
            this.buttonImg.Name = "buttonImg";
            this.buttonImg.Size = new System.Drawing.Size(190, 23);
            this.buttonImg.TabIndex = 3;
            this.buttonImg.Text = "Загрузить изображение";
            this.buttonImg.UseVisualStyleBackColor = true;
            this.buttonImg.Click += new System.EventHandler(this.ButtonImg_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(0, 440);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(487, 4);
            this.progressBar2.TabIndex = 4;
            // 
            // textBoxBaseUsers
            // 
            this.textBoxBaseUsers.Location = new System.Drawing.Point(21, 20);
            this.textBoxBaseUsers.Name = "textBoxBaseUsers";
            this.textBoxBaseUsers.ReadOnly = true;
            this.textBoxBaseUsers.Size = new System.Drawing.Size(225, 20);
            this.textBoxBaseUsers.TabIndex = 5;
            // 
            // textBoxBaseLogins
            // 
            this.textBoxBaseLogins.Location = new System.Drawing.Point(21, 45);
            this.textBoxBaseLogins.Name = "textBoxBaseLogins";
            this.textBoxBaseLogins.ReadOnly = true;
            this.textBoxBaseLogins.Size = new System.Drawing.Size(225, 20);
            this.textBoxBaseLogins.TabIndex = 6;
            // 
            // textBoxImg
            // 
            this.textBoxImg.Location = new System.Drawing.Point(21, 71);
            this.textBoxImg.Name = "textBoxImg";
            this.textBoxImg.ReadOnly = true;
            this.textBoxImg.Size = new System.Drawing.Size(225, 20);
            this.textBoxImg.TabIndex = 7;
            // 
            // textBoxTextMasseng
            // 
            this.textBoxTextMasseng.Location = new System.Drawing.Point(21, 152);
            this.textBoxTextMasseng.Multiline = true;
            this.textBoxTextMasseng.Name = "textBoxTextMasseng";
            this.textBoxTextMasseng.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxTextMasseng.Size = new System.Drawing.Size(448, 141);
            this.textBoxTextMasseng.TabIndex = 8;
            // 
            // textBoxSpammLog
            // 
            this.textBoxSpammLog.Location = new System.Drawing.Point(21, 310);
            this.textBoxSpammLog.Multiline = true;
            this.textBoxSpammLog.Name = "textBoxSpammLog";
            this.textBoxSpammLog.ReadOnly = true;
            this.textBoxSpammLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxSpammLog.Size = new System.Drawing.Size(448, 119);
            this.textBoxSpammLog.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(21, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(466, 37);
            this.label5.TabIndex = 10;
            this.label5.Text = "Введите текс отправляемого сообщения, можно использовать {fio} - это будет отправ" +
    "лять ФИО пользователя";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(226, 296);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Логи";
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(253, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 19);
            this.button3.TabIndex = 12;
            this.button3.Text = "X";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // SpammerVK
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(634, 461);
            this.Controls.Add(this.groupSpammerVK);
            this.Controls.Add(this.groupChecksAccountVK);
            this.Controls.Add(this.groupFindPeople);
            this.Controls.Add(this.buttonBox);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "SpammerVK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SpammerVK";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpammerVK_FormClosing);
            this.Load += new System.EventHandler(this.SpammerVK_Load);
            this.buttonBox.ResumeLayout(false);
            this.groupFindPeople.ResumeLayout(false);
            this.groupFindPeople.PerformLayout();
            this.groupFindPeople_1.ResumeLayout(false);
            this.groupFindPeople_1.PerformLayout();
            this.groupSpammerVK.ResumeLayout(false);
            this.groupSpammerVK.PerformLayout();
            this.groupChecksAccountVK.ResumeLayout(false);
            this.groupChecksAccountVK.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox buttonBox;
        private System.Windows.Forms.Button buttonChecksAccountVK;
        private System.Windows.Forms.Button buttonFindPeole;
        private System.Windows.Forms.Button buttonSpammerVK;
        private System.Windows.Forms.GroupBox groupFindPeople;
        private System.Windows.Forms.GroupBox groupFindPeople_1;
        private System.Windows.Forms.GroupBox groupSpammerVK;
        private System.Windows.Forms.GroupBox groupChecksAccountVK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Button buttonFindPeopleGo;
        private System.Windows.Forms.TextBox textBoxGroupVK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        public System.ComponentModel.BackgroundWorker GatheringGroupUsers;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.RadioButton radioButtonLikes;
        private System.Windows.Forms.RadioButton radioButtonAllUsersInGroup;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.TextBox textBoxQPosts;
        private System.Windows.Forms.Label labelQPosts;
        private System.Windows.Forms.RadioButton radioButtonComments;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxFileBase;
        private System.Windows.Forms.Button buttonBrute;
        private System.Windows.Forms.TextBox textBoxBruteLog;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxTextMasseng;
        private System.Windows.Forms.TextBox textBoxSpammLog;
        private System.Windows.Forms.TextBox textBoxImg;
        private System.Windows.Forms.TextBox textBoxBaseLogins;
        private System.Windows.Forms.TextBox textBoxBaseUsers;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Button buttonImg;
        private System.Windows.Forms.Button buttonBaseLogin;
        private System.Windows.Forms.Button buttonBaseUrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
    }
}

