﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SpammerVK_PageObject
{
    class PageObject
    {
        public IWebDriver driver;
        public IWebDriver WebDriverUp()
        {
            ChromeOptions options = new ChromeOptions();
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(@Environment.CurrentDirectory);
            service.SuppressInitialDiagnosticInformation = true;
            options.AddArguments("--disable-extensions");
            options.AddArgument("test-type");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("no-sandbox");
           // options.AddArgument("--headless");//hide browser
            service.HideCommandPromptWindow = true;
            return new ChromeDriver(service, options);
        }
        public void TakeMeDriver(IWebDriver driver)
        {
            this.driver = driver;
        }     
        public void InputElement(By elementLocator, string text)
        {
            IList<IWebElement> chooseElement = driver.FindElements(elementLocator);
            while (chooseElement.Count == 0)
            {
                chooseElement = driver.FindElements(elementLocator);
                Thread.Sleep(500);
            }
            driver.FindElement(elementLocator).SendKeys(text);
        }
        public void ClickElement(By elementLocator)
        {
            IList<IWebElement> chooseElement = driver.FindElements(elementLocator);
            while (chooseElement.Count == 0)
            {
                chooseElement = driver.FindElements(elementLocator);
                Thread.Sleep(500);
            }
            driver.FindElement(elementLocator).Click();
        }
        public void ClickElementWhithUseHref(By elementLocator)
        {
            IList<IWebElement> chooseElement = driver.FindElements(elementLocator);
            while (chooseElement.Count == 0)
            {
                chooseElement = driver.FindElements(elementLocator);
                Thread.Sleep(500);
            }
            var urlToClick = driver.FindElement(elementLocator).GetAttribute("href");
            driver.Navigate().GoToUrl(urlToClick);
            LoadingPageState(urlToClick);
        }
        public void ClickElementWhithUseHrefLastElements(By elementLocator)
        {
            IList<IWebElement> chooseElement = driver.FindElements(elementLocator);
            while (chooseElement.Count == 0)
            {
                chooseElement = driver.FindElements(elementLocator);
                Thread.Sleep(500);
            }
            var urlToClick = chooseElement.LastOrDefault().GetAttribute("href");
            driver.Navigate().GoToUrl(urlToClick);
            LoadingPageState(urlToClick);
        }
        public void GoToUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
            LoadingPageState(url);
        }
        public void ScrollingJS()
        {
            var scrollingJS = "var timeId = setInterval(function() {if (window.scrollY !== document.body.scrollHeight) window.scrollTo(0, document.body.scrollHeight); else clearInterval(timeId);}, 1000); ";
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(scrollingJS);
        }
        public void LoadingPageState()
        {
            var script = "return document.readyState === \"complete\"";
            var loading = (((IJavaScriptExecutor)driver).ExecuteScript(script)).ToString();
            while (loading != "True") loading = (((IJavaScriptExecutor)driver).ExecuteScript(script)).ToString();
        }
        public void LoadingPageState(string url)
        {
            var k = 0;
            var script = "return document.readyState === \"complete\"";
            var loading = (((IJavaScriptExecutor)driver).ExecuteScript(script)).ToString();
            while (loading != "True")
            {
                Thread.Sleep(500);
                loading = (((IJavaScriptExecutor)driver).ExecuteScript(script)).ToString();
                k++;
                if(k == 5)
                {
                    k = 0;
                    driver.Navigate().GoToUrl(url);
                }
            }
                
        }
        public void ScrollingJS_StaticPX()
        {
            var scrollingJS = "window.scrollBy(0,1000)";
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(scrollingJS);
        }
        public bool ChecksEllementInPage(By elementLocator)
        {
            IList<IWebElement> chooseElement = driver.FindElements(elementLocator);
            if (chooseElement.Count > 0)
                return true;
            else
                return false;
        }
    }
}
